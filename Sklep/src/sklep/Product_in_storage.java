package sklep;

public class Product_in_storage extends Product {
	int number_in_storage;
	//COnstructor
	public Product_in_storage(String type, String name, int buy_price, int sell_price,int number_in_storage) {
		super(type, name, buy_price, sell_price);
		this.number_in_storage = number_in_storage;
	}
	//overload
		public void display_information(){
			System.out.println("Product Type "+ this.type + " Product Name " + this.name +" Product buy_price " + this.buy_price + " Product Sell Price " + this.sell_price + " Product quantity in storage " + this.number_in_storage );
		}

}
