package sklep;

import java.util.ArrayList;

public class Shop {
	private String name;
	private int storage_space = 10;
	private int capital = 10000;
	ArrayList<Product_in_storage> Product_in_shop_storage = new ArrayList<Product_in_storage>();
	
	/*constructors*/
	public Shop(String name){
		this.name = name;
	}
	
	//Methods
	public void display_status(){
		System.out.println(" " );
		System.out.println(" " );
		System.out.println("Name " +this.name);
		System.out.println("Storage space "+ this.get_product_amount_in_storage_amount()+"/" +this.getStorage_space());
		System.out.println("Capital " +this.getCapital());
		menu.display_menu(this);
	}
	
	
	public void add_money(int money){
		this.setCapital(this.getCapital() + money);
		menu.display_menu(this);
	}
	
	public void buy_storage_space(int places){
		int money_needs_to_buy = places * 1000;
		
		
		/* TODO add wyjatek */
		if (this.getCapital() >= money_needs_to_buy){
			this.setCapital(this.getCapital() - money_needs_to_buy);
			this.setStorage_space(this.getStorage_space() + places);
			
		}else{
			System.out.println("You dont have enought money to buy storage spaces. Please try buy less storage spaces or earn more capital ");
		}
		menu.display_menu(this);
	
	}

	public int getCapital() {
		return capital;
	}

	public void setCapital(int capital) {
		this.capital = capital;
	}

	public int getStorage_space() {
		return storage_space;
	}

	public void setStorage_space(int storage_space) {
		this.storage_space = storage_space;
	}	
	
	public  int get_product_amount_in_storage_amount(){
		int product_amount=0;
		for(int i = 0; i < this.Product_in_shop_storage.size(); i++) {
			
			product_amount+= this.Product_in_shop_storage.get(i).number_in_storage;
			
        }
		return product_amount;
	}
}
