package sklep;

import java.util.HashMap;
import java.util.Map;

public class Deleted_product extends Product {
	Map<String, Integer> map = new HashMap<String, Integer>();
	Boolean is_deleted;

	//constructor
	public Deleted_product(String type, String name, int buy_price, int sell_price) {
		super(type, name, buy_price, sell_price);
		this.is_deleted = true;
		
		// TODO Auto-generated constructor stub
	}
	//method
	public void display_information(){
		System.out.println(" Product Name " + this.name +"was deleted" );

	}
	//add to deleted map
	public void add_to_deleted_map(Product product, Map<String, Product> map){
		map.put(product.name, product  );
		
	};
	
	public void check_are_in_deleted(Map<String, Product> map, String text){
	    if (map.get(text) != null){
	    	System.out.println(map.get(text));
	    }else{
	    	System.out.println("This obiect does not existing in deleted list");

	    }

		
	}


}
