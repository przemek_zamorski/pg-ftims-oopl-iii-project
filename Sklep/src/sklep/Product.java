package sklep;

public class Product extends Product_Type {
	protected String name;
	protected int buy_price;
	protected int sell_price;
	
	/*constructors*/

	public Product(String type, String name, int buy_price, int sell_price) {
		super(type);
		this.name = name;
		this.buy_price = buy_price;
		this.sell_price = sell_price;
				
	}
	//Methods
	//overload
	public void display_information(){
		System.out.println("Product Type "+ this.type + " Product Name " + this.name +" Product buy_price " + this.buy_price + " Product Sell Price " + this.sell_price  );
	}

	
}
